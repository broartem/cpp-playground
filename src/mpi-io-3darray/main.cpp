#include <iostream>
#include <limits>
#include <bits/stl_bvector.h>
#include <math.h>
#include "mpi.h"

using xyz = std::tuple<MPI::Offset, MPI::Offset, MPI::Offset>;

void write_file_part(const char *filename)
{
    // TODO
}

void read_file_part(const char *filename)
{
    const int prank = MPI::COMM_WORLD.Get_rank();
    const int psize = MPI::COMM_WORLD.Get_size();

    auto file = MPI::File::Open(
        MPI::COMM_WORLD,
        filename,
        MPI::MODE_RDONLY,
        MPI::INFO_NULL);

    const auto int_size = static_cast<MPI::Offset>(sizeof(int));
    const auto filesize = file.Get_size() / int_size;

    const auto unbounded_bufsize = filesize / psize + 1;

    if (unbounded_bufsize > std::numeric_limits<int>::max())
        throw "Bufer size is too big. You need more processors to read this file";

    const auto bufsize = static_cast<int>(unbounded_bufsize);
    const auto buf = new int [bufsize];
    auto buf_offset = static_cast<MPI::Offset>(prank * bufsize * sizeof(int));

    file.Set_view(buf_offset, MPI_INT, MPI_INT, "native", MPI::INFO_NULL);

    MPI::Status status;
    file.Read(buf, bufsize, MPI_INT, status);

    const auto count = status.Get_count(MPI_INT);
    std::cout << "process " << prank << " read " << count << " ints" << std::endl;
    file.Close();
}

xyz index2xyz(MPI::Offset index,
              MPI::Offset xdim,
              MPI::Offset ydim,
              MPI::Offset zdim)
{
    auto z = index % xdim;
    index /= xdim;
    auto y = index % ydim;
    auto x = index / ydim;
    return std::make_tuple(x, y, z);
}

std::vector<double> make3d_dataset(MPI::Offset offset, int size, int xdim, int ydim, int zdim)
{
    std::vector<double> buf (static_cast<unsigned long>(size));
    for (int i = 0; i < size; ++i)
    {
        const auto p = index2xyz(offset + i, xdim, ydim, zdim);
        buf[i] = sin(std::get<0>(p) * std::get<1>(p) * std::get<2>(p));
    }
    return buf;
}

int main(int argc, char *argv[])
{
    MPI::Init();
    read_file_part("/home/broartem/Development/cpp/cpp-playground/src/mpi-io-3darray/testdata");
    MPI::Finalize();
    return 0;
} 