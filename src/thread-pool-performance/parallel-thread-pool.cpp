#include <iostream>
#include <functional>
#include "ThreadPool.h"
#include "parser.h"

int main(int argc, char const *argv[])
{
    if (argc < 2) {
        std::cout << "File name is not specified!\n";
        return 0;
    }

    // create thread pool with 4 worker threads
    ThreadPool pool(4);

    auto scheduler = [&pool] (ParserFn parser, std::string line) {
        return pool.enqueue(parser, line).get();
        // Vec x (0);
        // return x;
    };

    Vec2 result = parse_file(argv[1], scheduler);

    // for (auto &v1 : result) {
    //     std::cout << v1[0] << " " << v1[1] << std::endl;
    // }
    

    // auto result = enqueue([](int answer) { return answer; }, 42);

    // enqueue and store future
    // auto result = pool.enqueue([](std::string line) { return answer; }, 42);

    // get result from future
    // std::cout << result.get() << std::endl;
    return 0;
}