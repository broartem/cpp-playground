#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <functional>
#include <stdexcept>
#include <chrono>

typedef int element_t;
using Vec = std::vector<element_t>;
using Vec2 = std::vector<Vec>;

// Type for a scheduler function, which takes parser (e.g. parse_line) as its first
// argument and line to parse as the second one
using ParserFn = std::function<Vec(std::string)>;
using ShedulerFn = std::function<Vec(ParserFn,std::string)>;

Vec parse_line(std::string line)
{
    std::istringstream iss(line);
    Vec result (2);

    if (!(iss >> result[0] >> result[1])) {
        throw std::invalid_argument(line);
    }
    return result;
}

Vec2 parse_file(std::string file_name, ShedulerFn scheduler)
{
    std::ifstream is(file_name);
    Vec2 result;
    std::string line;

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    int i = 0;
    while (getline(is, line)) {
        result.push_back(scheduler(parse_line, line));

        ++i;
        if (i % 1000000 == 0)
            std::cout << i/1000000 << "M rows are read\n";
    }

    end = std::chrono::system_clock::now();
 
    std::chrono::duration<double> elapsed_seconds = end-start;
 
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
    return result;
}