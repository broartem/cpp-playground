#include <iostream>
#include <functional>
#include "ThreadPool.h"
#include "parser.h"

int main(int argc, char const *argv[])
{
    if (argc < 2) {
        std::cout << "File name is not specified!\n";
        return 0;
    }

    auto scheduler = [] (ParserFn parser, std::string line) {
        return parser(line);
    };

    Vec2 result = parse_file(argv[1], scheduler);

    // for (auto &v1 : result) {
    //     std::cout << v1[0] << " " << v1[1] << std::endl;
    // }
    return 0;
}