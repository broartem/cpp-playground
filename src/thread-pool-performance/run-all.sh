#!/bin/bash

# NOTE: build/ directory is added to the project's .gitignore file
# so it is OK to create this folder right there
mkdir build/
cd build

cmake ..
make

if [ ! -f DirectedGraphEdges875714.txt ]; then
    # Getting a big file from https://github.com/broartem/various-datasets
    wget -O DirectedGraphEdges875714.zip \
        https://raw.githubusercontent.com/broartem/various-datasets/master/data/DirectedGraphEdges875714.zip; \
        unzip DirectedGraphEdges875714.zip; \
        rm DirectedGraphEdges875714.zip
fi

printf "\n\n-------------- Testing on file %s/DirectedGraphEdges875714.txt-------------------------" "${PWD##*/}"

# lets start with a serial version
printf "\n\n************ Running serial version:\n\n"
./serial DirectedGraphEdges875714.txt

# lets start with a serial version
printf "\n\n************ Running parallel version (using thread pool):\n\n"
./parallel-thread-pool DirectedGraphEdges875714.txt

printf "\n"
