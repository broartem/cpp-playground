// http://stackoverflow.com/questions/17465061
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/home/support/info.hpp>
//#include <boost/iostreams/device/mapped_file.hpp>

namespace qi = boost::spirit::qi;

using boost::spirit::qi::expectation_failure;
using boost::spirit::info;

struct double3
{
    double x,y,z;
};

BOOST_FUSION_ADAPT_STRUCT(double3, (double, x)(double, y)(double, z))

typedef std::vector<double3> data_t;

/////////////////////////////////////////////////
// For exception output
struct printer {
    typedef boost::spirit::utf8_string string;

    void element(string const& tag, string const& value, int depth) const {
        for (int i = 0; i < (depth*4); ++i) std::cout << ' '; // indent to depth

        std::cout << "tag: " << tag;
        if (value != "") std::cout << ", value: " << value;
        std::cout << std::endl;
    }
};

void print_info(boost::spirit::info const& what) {
    using boost::spirit::basic_info_walker;

    printer pr;
    basic_info_walker<printer> walker(pr, what.tag, 0);
    boost::apply_visitor(walker, what.value);
}
//
/////////////////////////////////////////////////

// std::vector<double3> parse_coordinates(const char *fname)
// {
//     using namespace qi;
//     std::vector<double3> coordinates;
//     coordinates.reserve(11000000);
//
//     boost::iostreams::mapped_file mmap(fname,
//         boost::iostreams::mapped_file::readonly);
//     auto f = mmap.const_data();
//     auto l = f + mmap.size();
//
//     bool ok = phrase_parse(f,l,(double_ > double_ > double_) % eol, blank, coordinates);
//     if (ok)
//         std::cout << "parse success\n";
//     else
//         std::cerr << "parse failed: '" << std::string(f,l) << "'\n";
//
//     if (f!=l) std::cerr << "trailing unparsed: '" << std::string(f,l) << "'\n";
// }

int main(int argc, char const *argv[]) {
    if (argc < 1) {
        std::cout << "File name must be set";
        return 0;
    }
    typedef std::string::const_iterator It;
    std::string nums = "  1,20890625e+17       -34844,48  -2,9098902e-11";
    data_t data;
    data.reserve(1);

    try
    {
        bool ok = qi::phrase_parse(
            nums.begin(),
            nums.end(),
            (qi::double_ > qi::double_ > qi::double_) % qi::eol,
            qi::blank,
            data);
    }
    catch (qi::expectation_failure<It> const& x)
    {
        std::cout << "expected: "; print_info(x.what_);
        std::cout << "got: \"" << std::string(x.first, x.last) << '"' << std::endl;
    }
    // if (ok)
    //     std::cout << "parse success\n";
    // else
    //     std::cerr << "parse failed" << "\n";
    // std::cout << data.x << " " << data.y << " " << data.z << "\n";
    return 0;
}
