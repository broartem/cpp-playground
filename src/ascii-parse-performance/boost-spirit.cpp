#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

namespace qi = boost::spirit::qi;

#if 1
    struct float3
    {
        float x,y,z;
    };

    BOOST_FUSION_ADAPT_STRUCT(float3, (float, x)(float, y)(float, z))

    typedef std::vector<float3> data_t;
#else
    // there is no speed difference when parsing into a straight container
    // (only works with the spirit implementation)
    typedef std::vector<float> data_t;
#endif

int main()
{
#if 0
    std::cin.unsetf(std::ios::skipws);
    std::istreambuf_iterator<char> f_(std::cin), l_;

    const std::vector<char> v(f_, l_);
    auto f = v.data(), l = f+v.size();
#elif 1
    boost::iostreams::mapped_file mmap(
            "input.txt",
            boost::iostreams::mapped_file::readonly);
    auto f = mmap.const_data();
    auto l = f + mmap.size();
#endif

    data_t data;
    data.reserve(11000000);

#if 1
    using namespace qi;
    bool ok = phrase_parse(f,l,(double_ > double_ > double_) % eol, blank, data);
    if (ok)
        std::cout << "parse success\n";
    else
        std::cerr << "parse failed: '" << std::string(f,l) << "'\n";

    if (f!=l) std::cerr << "trailing unparsed: '" << std::string(f,l) << "'\n";
#elif 1
    errno = 0;
    char* next = nullptr;
    float3 tmp;
    while (errno == 0 && f && f<(l-12) ) {
        tmp.x = strtod(f, &next); f = next;
        tmp.y = strtod(f, &next); f = next;
        tmp.z = strtod(f, &next); f = next;
        data.push_back(tmp);
    }
#else
    FILE* file = fopen("input.txt", "r");
    if (NULL == file) {
        printf("Failed to open 'input.txt'");
        return 255;
    }
    float3 tmp;
    do {
        int nItemsRead = fscanf(file,"%f %f %f\n", &tmp.x, &tmp.y, &tmp.z);
        if (3 != nItemsRead)
            break;
        data.push_back(tmp);
    } while (1);

#endif

    std::cout << "data.size():   " << data.size() << "\n";
}
