#include <iostream>
#include <fstream>
#include <vector>
#include <cerrno>
#include <cstdlib>
#include <memory>

struct point_t
{
    double x,y,z;
};

char* fread2str(const char *fname, int& length)
{
    std::ifstream file;
    file.open(fname);
    file.seekg(0, std::ios::end);
    length = file.tellg();
    file.seekg(0, std::ios::beg);

    char *buffer = new char[length];
    file.read(buffer, length);
    file.close();
    return buffer;
}

std::vector<point_t> parse_coordinates(const char *fname)
{
    std::vector<point_t> coordinates;
    int length = 0;
    char *buffer = fread2str(fname, length);

    errno = 0;
    char* next = nullptr;
    point_t tmp;
    auto f = buffer;
    auto l = buffer + length;
    while (errno == 0 && f && f<(l-12) ) {
        tmp.x = strtod(f, &next); f = next;
        tmp.y = strtod(f, &next); f = next;
        tmp.z = strtod(f, &next); f = next;
        coordinates.push_back(tmp);
    }
    // delete buffer;
    return coordinates;
}

int main(int argc, char const *argv[]) {
    if (argc < 1) {
        std::cout << "File name must be set";
        return 0;
    }
    std::vector<point_t> coordinates = parse_coordinates(argv[1]);
    auto size = coordinates.size();
    point_t last = coordinates.at(size-1);
    std::cout << "Size: " << size << std::endl;

    // Allow user to check that input file is parsed correctly up untill the last line
    std::cout << "Last: (" << last.x << "," << last.y << "," << last.z << ")" << std::endl;
    return 0;
}
