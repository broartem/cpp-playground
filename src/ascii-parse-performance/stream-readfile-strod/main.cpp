#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cerrno>
#include <cstdlib>

struct point_t
{
    double x,y,z;
};

std::vector<point_t> parse_coordinates(const char *fname)
{
    std::vector<point_t> coordinates;
    std::ifstream t(fname);
    std::stringstream bufferstream;
    bufferstream << t.rdbuf();
    std::string strbuffer = bufferstream.str();
    const char *buffer = strbuffer.c_str();
    auto length = strbuffer.size();

    errno = 0;
    char* next = nullptr;
    point_t tmp;
    auto f = buffer;
    auto l = buffer + length;
    while (errno == 0 && f && f<(l-12) ) {
        tmp.x = strtod(f, &next); f = next;
        tmp.y = strtod(f, &next); f = next;
        tmp.z = strtod(f, &next); f = next;
        coordinates.push_back(tmp);
    }
    return coordinates;
}

int main(int argc, char const *argv[]) {
    if (argc < 1) {
        std::cout << "File name must be set";
        return 0;
    }
    std::vector<point_t> coordinates = parse_coordinates(argv[1]);
    auto size = coordinates.size();
    point_t last = coordinates.at(size-1);
    std::cout << "Size: " << size << std::endl;

    // Allow user to check that input file is parsed correctly up untill the last line
    std::cout << "Last: (" << last.x << "," << last.y << "," << last.z << ")" << std::endl;
    return 0;
}
