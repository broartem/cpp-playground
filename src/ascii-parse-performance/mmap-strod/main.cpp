#include <boost/iostreams/device/mapped_file.hpp>

struct point_t
{
    double x,y,z;
};

std::vector<point_t> parse_coordinates(const char *fname)
{
    std::vector<point_t> coordinates;

    boost::iostreams::mapped_file mmap(fname,
        boost::iostreams::mapped_file::readonly);
    auto f = mmap.const_data();
    auto l = f + mmap.size();

    errno = 0;
    char* next = nullptr;
    point_t tmp;
    while (errno == 0 && f && f<(l-12) ) {
        tmp.x = strtod(f, &next); f = next;
        tmp.y = strtod(f, &next); f = next;
        tmp.z = strtod(f, &next); f = next;
        coordinates.push_back(tmp);
    }
    return coordinates;
}

int main(int argc, char const *argv[]) {
    if (argc < 1) {
        std::cout << "File name must be set";
        return 0;
    }
    std::vector<point_t> coordinates = parse_coordinates(argv[1]);
    auto size = coordinates.size();
    point_t last = coordinates.at(size-1);
    std::cout << "Size: " << size << std::endl;

    // Allow user to check that input file is parsed correctly up untill the last line
    std::cout << "Last: (" << last.x << "," << last.y << "," << last.z << ")" << std::endl;
    return 0;
}
